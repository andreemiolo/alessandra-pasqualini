<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'a_p');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4ZD><14F2h`kZ@{1o}C0y-ds~2$wEK4`VP9W*S*F!5C+)%}1wvNXuCP 9W]X!?K0');
define('SECURE_AUTH_KEY',  'HocJ2X_/f9<^{ncw<Ae!#Oy=EFX$5y29Az=x.!%.V(+K$b7eHz6=W%eM )7L7:uF');
define('LOGGED_IN_KEY',    'iO}om/*nEk21ke5XwF)LVkQ#_$m8u%9=9El`+Usu%(k4q{XxDL_y<_sU y34bdJQ');
define('NONCE_KEY',        'l[>/xe_FT1*?1rRJkU.F{uYsG..!:]*H6?/szlD)*^P8<PD4u/|IeD7# G-m>Rs:');
define('AUTH_SALT',        '`8ZBC251=r.5$OqSP`WsH$6DEGzkt{`tMAuM8jBtjX(f9 ~zH|9R>*F-H9(&KoNt');
define('SECURE_AUTH_SALT', ',[V|sq&Q%?zi,uo1Vs3WWj@_2~:VP+IZt{^jIKM$<Z?.AK(~,g);0X+V#xb~`;mx');
define('LOGGED_IN_SALT',   'n5qH@dfqkt$-?ZqL{5+rO]nlhRBX{cfQ}D1MJchg_DoTFcd x93n+)#hB=7Ig*X-');
define('NONCE_SALT',       '2c}5Wr|MykE6+ILQO)=%,Q/6h5lxt^GA( %2s:@]R]t+7-ne!nuFtBv^&%2daHNB');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
